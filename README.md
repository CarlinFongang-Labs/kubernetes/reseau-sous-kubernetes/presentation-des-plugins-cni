# Présentation des plugins CNI
------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Aperçu des plugins CNI

Nous allons fournir un aperçu rapide du concept des plugins CNI. Voici un résumé des sujets que nous allons aborder :

1. Le concept des plugins CNI
2. Le processus de sélection d'un plugin réseau
3. Le processus d'installation des plugins réseau

# Qu'est-ce que les plugins CNI ?

Les plugins CNI (Container Network Interface) sont un type de plugin réseau Kubernetes. Ces plugins fournissent la connectivité réseau entre les pods, conformément aux normes établies par le modèle réseau de Kubernetes. Dans la leçon précédente, nous avons parlé du modèle réseau où chaque pod a sa propre adresse IP unique dans le cluster et peut communiquer avec les autres de manière transparente, quel que soit le nœud sur lequel ils s'exécutent. Les plugins CNI sont les plugins qui implémentent ce modèle et le font fonctionner.

# Sélection d'un plugin réseau

Il existe de nombreux plugins réseau CNI disponibles. Comment choisir celui que vous souhaitez utiliser ? Il y a tellement de plugins réseau que nous ne pourrons pas entrer dans les détails de chacun. L'idée de base est de consulter la documentation Kubernetes pour une liste des plugins disponibles. Vous devrez peut-être rechercher certains de ces plugins vous-même, en fonction de vos cas d'utilisation en production. Le plugin utilisé dans ce cours est Calico. Si vous voulez utiliser Calico, vous pouvez probablement le faire dans la plupart des situations. Mais si vous avez des cas d'utilisation spécifiques dans le monde réel et que ce plugin particulier ne fonctionne pas, vous devrez peut-être examiner d'autres plugins réseau disponibles.

# Installation des plugins réseau

Le processus d'installation des plugins réseau est unique à chaque plugin. Au début de ce cours, nous avons installé le plugin réseau Calico lors de la création initiale de notre cluster que nous avons utilisé tout au long du cours. Une dernière chose que je veux que vous sachiez, et que vous avez peut-être déjà vue en suivant ce cours, est que les nœuds Kubernetes resteront dans l'état **NotReady** jusqu'à ce qu'un plugin réseau soit installé. Cela signifie que vous ne pourrez pas exécuter de pods dans votre cluster tant que vous n'aurez pas installé ce plugin réseau, car aucun des nœuds ne sera prêt. Par conséquent, tous les pods que vous essayez de créer resteront dans l'état **Pending**, en attente qu'un nœud devienne prêt.

# Conclusion

Pour récapituler, nous avons discuté du concept des plugins CNI, de la sélection d'un plugin réseau et nous avons parlé à un niveau très général du processus d'installation des plugins réseau. C'est tout pour cette leçon. À la prochaine !


# Reférence

https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/network-plugins/